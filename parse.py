#!/usr/bin/env python

import csv, sys

csvReader = csv.reader( sys.stdin )

#throw away heading
csvReader.next()
csvReader.next()

loanStatuses = {
    "Default":"Ignore",
    "In Review":"Ignore",
    "Issued":"Ignore",
    "Current":"Good",
    "In Grace Period":"Bad",
    "Late (16-30 days)":"Bad",
    "Late (31-120 days)":"Bad",
    "Charged Off":"Bad",
    "Fully Paid":"Good"
}

CREDIT_GRADE = 8
STATUS = 13

class pcalc:
    def __init__(self):
        self.counts = {}
        self.catCounts = {}

    def addPoint(self, value, classification):
        self.counts.setdefault(value, 0)
        self.counts[value] += 1

        self.catCounts.setdefault(value, {})
        self.catCounts[value].setdefault(classification, 0)
        self.catCounts[value][classification] += 1

    def debug(self):
        print self.counts
        print self.catCounts

    def pInClass(self, value, classification):
        if value not in self.counts:
            return 0.0

        return self.catCounts[value][classification] / float(self.counts[value])


classifier = pcalc()


for line in csvReader:
    if len(line) < 14:
        continue

    #remove some cases where status has decorator
    if line[STATUS].find('Does not meet the current credit policy  Status: ') == 0:
        line[STATUS] = line[STATUS][len('Does not meet the current credit policy  Status: '):]

    outRow = [
        line[CREDIT_GRADE][0],
        loanStatuses[line[STATUS]]
    ]

    if outRow[1] =="Ignore": 
        continue

    # print "\t".join( outRow )

    classifier.addPoint(outRow[0], outRow[1])

classifier.debug()


for f in "A B C D E F G".split(" "):
    print f, classifier.pInClass(f, "Bad")




