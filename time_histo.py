#!/usr/bin/env python

import csv, sys, time

csvReader = csv.reader( sys.stdin )

#throw away heading
csvReader.next()
csvReader.next()

histo = {}

for line in csvReader:
    if len(line) < 14:
        continue

    line[5] = line[5][:8] + "01"

    histo.setdefault(line[5], 0)
    histo[line[5]] += 1


for key in sorted(histo.iterkeys()):
    print "%s\t%d" % (key, histo[key])
